#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <pulse/pulseaudio.h>

#include "../include/Client.h"

struct DataNode {
    void *data;
    size_t len;
    struct DataNode *next;
};

int main() {
    printf("Starting!\n");

    int socketFD = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr("192.168.1.5");
    addr.sin_port = htons(PORT);

    int status = connect(socketFD, (struct sockaddr *) &addr, sizeof(addr));

    printf("Status: %d\n", status);

    FILE *music = fopen("/home/poku/CLionProjects/AudioStreamPC/Ambulance.wav", "r");
    if (!music) {
        perror("Music is null");
        return EXIT_FAILURE;
    }

    struct DataNode *dataNode = calloc(1, sizeof(struct DataNode));
    struct DataNode *lastNode = NULL;

    while (1) {
        int16_t *data = calloc(1024, sizeof(int16_t));
        size_t len = fread(data, sizeof(int16_t), 1024, music);
        if (len <= 0)
            break;
        if (lastNode == NULL)  {
            lastNode = dataNode;
        } else {
            lastNode = lastNode->next = calloc(1, sizeof(struct DataNode));
        }
        lastNode->data = data;
        lastNode->len = len;
    }
    
    for (struct DataNode *e = dataNode; e != NULL; e = e->next) {
        send(socketFD, e->data, e->len, 0);
    }
    printf("Finished\n");
    return 0;
}
