//
// Created by poku on 8/27/19.
//

#include <nds.h>
#include <maxmod9.h>
#include <stdio.h>
#include "../include/Audio.h"

static bool initialized = false;
static mm_ds_system sys;
static mm_stream stream;

struct AudioData {
    void *data;
    u32 dataSize;
    u32 left;
    struct AudioData *next;
};

struct AudioData *audioBuffer = NULL;

static mm_word updateStream(mm_word length, mm_addr dest, mm_stream_formats format) {
    if (format !=  MM_STREAM_16BIT_STEREO) {
        iprintf("Format is not 16Bit Stereo (%d): %d\nReturning 0\n", MM_STREAM_16BIT_STEREO, format);
        return 0;
    }

    s16 *target = dest;
    mm_word sampleLength = 0;

    if (audioBuffer != NULL) {
        // copy data to memory
        for (; sampleLength < length && audioBuffer->left > 0; sampleLength++)
            *(target + sampleLength) = ((s16 *) audioBuffer->data)[audioBuffer->dataSize - audioBuffer->left--];
        // free memory and next audio track
        if (audioBuffer->left == 0) {
            struct AudioData *next = audioBuffer->next;
            free(audioBuffer->data);
            free(audioBuffer);
            audioBuffer = next;
        }
    }

    iprintf("Playing length: %d\n", sampleLength);

    return sampleLength;
}

void audioInit() {
    if (initialized)
        return;
    // initialize MaxMod
    sys.mod_count    = 0;
    sys.samp_count   = 0;
    sys.mem_bank     = 0;
    sys.fifo_channel = FIFO_MAXMOD;
    mmInit(&sys);
    // open stream
    stream.sampling_rate = 8000;
    stream.buffer_length = 1200;
    stream.callback      = updateStream;
    stream.format        = MM_STREAM_16BIT_STEREO;
    stream.timer         = MM_TIMER0;
    stream.manual        = false;
    mmStreamOpen(&stream);

    initialized = true;
}

void playData(void *data, u32 dataSize) {
    if (initialized) {
        struct AudioData *audio = malloc(sizeof(struct AudioData));
        audio->data = data;
        audio->dataSize = audio->left = dataSize;
        audio->next = NULL;

        struct AudioData *e, *oldE;
        if ((e = audioBuffer) != NULL) {
            do {
                oldE = e;
            } while ((e = e->next) != NULL);
            oldE->next = audio;
        } else {
            audioBuffer = audio;
        }
        iprintf("Buffered\n");
    } else {
        iprintf("Audio is not initialized!\n");
    }
}