//
// Created by root on 8/24/19.
//

#include "../include/Initializer.h"
#include "../include/Audio.h"

#include <stdio.h>
#include <stdbool.h>
#include <nds/interrupts.h>

static bool running = false;
static uint8_t lastId = 0;

void start(void (*runner)(void)) {
    // generate ID
    uint8_t id = ++lastId;

    if (running) {
        // notify if it's resetting
        iprintf("Initializer resetting...\n");
    }

    running = true;

    // notify started state has begun
    iprintf("Initializer started\n");
    audioInit();
    iprintf("Audio initialized\n");
    while (running) {
        // do what program requires
        runner();
        // check if it wasn't started again...
        if (lastId  != id)
            break;
        swiWaitForVBlank();
    }

    if (!(running) && id == lastId)
        lastId = 0;
}

void stop() {
    running = false;
    iprintf("Initializer stopped\n");
}