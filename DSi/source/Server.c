//
// Created by poku on 8/26/19.
//

#include <nds.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <dswifi9.h>
#include <stdlib.h>
#include <stdio.h>

#include "../include/Server.h"

struct Server {
    int serverFileDescriptor;
    struct sockaddr_in *addr;
    int clientFileDescriptor;
    struct ClientData *clientData;
    void (*originalTicker)(void);
};

static bool wifi_initialized = false;
static struct TickData *actualData = NULL;

static bool connectWifi() {
    if (!(wifi_initialized))
        wifi_initialized = Wifi_InitDefault(WFC_CONNECT);
    Wifi_AutoConnect();
    iprintf("Press A to exit...\n");
    int status;
    do {
        status = Wifi_AssocStatus();
        if (status == ASSOCSTATUS_ASSOCIATED) {
            return true;
        } else if (status == ASSOCSTATUS_CANNOTCONNECT) {
            return false;
        } else {
            scanKeys();
            if ((uint32_t) KEY_A & keysHeld())
                break;
        }
    } while (1);
    return false;
}

size_t writeData(void *server, s16 *data, size_t len) {
    return send(((struct Server *) server)->clientFileDescriptor, data, len, 0);
}

size_t readData(void *server, s16 *data, size_t len) {
    return recv(((struct Server *) server)->clientFileDescriptor, data, len, 0);
}

void disconnect(void *server) {
    struct Server *s = server;
    closesocket(s->clientFileDescriptor);
    s->clientFileDescriptor = 0;
    free(s->clientData);
    s->clientData = NULL;
}

void serverTicker(void) {
    if (actualData == NULL)
        return;
    struct Server *server = ((struct Server *) actualData->server);
    if (server->clientFileDescriptor == 0) {
        iprintf("Waiting client...\n");
        size_t addrLen = sizeof(server->addr);
        server->clientFileDescriptor = accept(server->serverFileDescriptor, (struct sockaddr *) server->addr, (int *)&addrLen);
        if (server->clientFileDescriptor == 0)
            iprintf("Client couldn't connect!\n");
        else {
            server->clientData = malloc(sizeof(struct ClientData));
            server->clientData->writeData = writeData;
            server->clientData->readData = readData;
            server->clientData->disconnect = disconnect;
            iprintf("Client has connected!\n");
        }
    } else {
        server->originalTicker();
    }
}

int startServer(Ticker ticker) {
    iprintf("Waiting WFC data...\n");
    if (!(connectWifi()))
      return WIFI_INIT_ERROR;
    if (actualData != NULL)
        return ALREADY_CREATED;
    actualData = calloc(1, sizeof(struct TickData));
    struct Server *server;
    server = actualData->server = calloc(1, sizeof(struct Server));
    server->clientFileDescriptor = 0;
    if ((server->serverFileDescriptor = socket(AF_INET, SOCK_STREAM, 0)) != 0) {
        server->addr = malloc(sizeof(struct sockaddr_in));
        server->addr->sin_family = AF_INET;
        server->addr->sin_addr.s_addr = INADDR_ANY;
        server->addr->sin_port = htons(PORT);
        if (bind(server->serverFileDescriptor, (struct sockaddr *)server->addr, sizeof(*server->addr)) < 0) {
            free(server->addr);
            free(server);
            free(actualData);
            actualData = NULL;
            return BIND_ERROR;
        }
        if (listen(server->serverFileDescriptor, 1) < 0) {
            free(server->addr);
            free(server);
            free(actualData);
            actualData = NULL;
            return LISTEN_ERROR;
        }
    } else {
        free(server);
        free(actualData);
        actualData = NULL;
        return SOCK_ERROR;
    }
    server->originalTicker = ticker;
    actualData->ticker = serverTicker;
    return SUCCESSFULLY_SERVER;
}

struct TickData *getTickData() {
    return actualData;
}

bool isValid(struct TickData *tickData) {
    return tickData->server == actualData->server;
}

bool isConnected() {
    return wifi_initialized && Wifi_AssocStatus() == ASSOCSTATUS_ASSOCIATED;
}

struct ClientData *getClient(struct TickData *tickData) {
    if (isValid(tickData)) {
        struct Server *server = ((struct Server *) tickData->server);
        if (server->clientFileDescriptor != 0)
            return server->clientData;
    }
    return NULL;
}


void stopServer() {
    if (actualData == NULL
       || ((struct Server *) actualData->server)->serverFileDescriptor == 0)
        return;

    void *p;
    if ((p = ((struct Server *) actualData->server)->clientData) != NULL)
        free(p);
    free(actualData->server);
    free(actualData);
    actualData = NULL;

    wifi_initialized = false;
    Wifi_DisconnectAP();
}