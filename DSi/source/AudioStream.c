#include <nds.h>
#include <stdio.h>

#include "../include/Initializer.h"
#include "../include/Server.h"
#include "../include/Audio.h"

static uint32_t heldKey;
static struct TickData *serverData = NULL;

void runGUI(void);
void runServer(void);

int main() {
    if (consoleDemoInit() == NULL) {
        perror("Console couldn't initialize");
        return EXIT_FAILURE;
    }
    if (keyboardDemoInit() == NULL) {
        perror("Keyboard couldn't initialize");
        return EXIT_FAILURE;
    }

    start(runGUI);

    iprintf("Program finished successfully\n");
    swiWaitForVBlank();

    swiIntrWait(5, 0);

    return EXIT_SUCCESS;
}

void runGUI(void) {
    scanKeys();
    heldKey = keysHeld();

    if ((uint32_t) KEY_START & heldKey) {
        stop();
        stopServer();
        return;
    }

    if (serverData == NULL) {
        if ((uint32_t) KEY_SELECT & heldKey) {
            iprintf("Creating server thread...\n");
            runServer();
        }
    } else {
        runServer();
    }
}

void tickServer(void) {
    struct ClientData *clientData;
    if ((clientData = getClient(serverData)) == NULL)
        return;

    if ((uint32_t) KEY_B & heldKey) {
        clientData->disconnect(serverData->server);
        iprintf("Client disconnected\n");
        return;
    }

    s16 *data = calloc(AUDIO_BUFFER_LENGTH, sizeof(s16));
    int recvLen = clientData->readData(serverData->server, data, AUDIO_BUFFER_LENGTH);
    if (recvLen > 0)
        playData(data, recvLen);
}

void runServer(void) {
    ServerDataSearch:
    if (serverData == NULL) {
        int serverStatus;
        if ((serverStatus = startServer(tickServer)) == SUCCESSFULLY_SERVER) {
            if ((serverData = getTickData()) == NULL) {
                iprintf("Tick data is null\n");
                return;
            }
            iprintf("Server has been created\n");
        } else if (serverStatus == ALREADY_CREATED) {
            iprintf("Server was already created! Getting data\n");
            serverData = getTickData();
        } else {
            iprintf("Server couldn't initialize. Status code: %d\n", serverStatus);
            return;
        }
    } else if (!(isValid(serverData))) {
        serverData = NULL;
        iprintf("Server has fell. Trying to reconnect\n");
        stopServer();
        goto ServerDataSearch;
    }

    serverData->ticker();
}