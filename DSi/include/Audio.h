//
// Created by poku on 8/27/19.
//

#ifndef AUDIOSTREAM_AUDIO_H
#define AUDIOSTREAM_AUDIO_H

#define AUDIO_BUFFER_LENGTH 1024

#include <maxmod9.h>
#include <nds.h>

void audioInit();

void playData(void *data, u32 dataSize);

#endif //AUDIOSTREAM_AUDIO_H
