//
// Created by poku on 8/26/19.
//

#ifndef AUDIOSTREAM_SERVER_H
#define AUDIOSTREAM_SERVER_H

#include <stdint.h>
#include <stdbool.h>

#define PORT 7823

#define SUCCESSFULLY_SERVER 0x000000
#define WIFI_INIT_ERROR     0x000001
#define SOCK_ERROR          0x000002
#define BIND_ERROR          0x000003
#define LISTEN_ERROR        0x000004
#define ALREADY_CREATED     0x000005

typedef void (*Ticker)(void);

struct ClientData {
    size_t (*writeData)(void *server, s16 *data, size_t len);
    size_t (*readData)(void *server, s16 *data, size_t len);
    void (*disconnect)(void *server);
};

struct TickData {
    Ticker ticker;
    void *server;
};

int startServer(void (*ticker)(void));

struct TickData *getTickData();

bool isValid(struct TickData *tickData);

bool isConnected();

// null if there is no
struct ClientData *getClient(struct TickData *tickData);

void stopServer();

#endif //AUDIOSTREAM_SERVER_H
