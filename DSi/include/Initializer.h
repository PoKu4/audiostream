//
// Created by poku on 8/26/19.
//

#ifndef AUDIOSTREAM_INITIALIZER_H
#define AUDIOSTREAM_INITIALIZER_H

void start(void (*runner)(void));

void stop();

#endif //AUDIOSTREAM_INITIALIZER_H
